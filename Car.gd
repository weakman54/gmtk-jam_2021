extends Area2D

class_name Car

signal attachEntered(top, bottom)

var isAttached : bool = false;

onready var frontPoint = $FrontAttach
onready var backPoint  = $BackAttach


func _on_AttachFront_area_entered(other : Area2D) -> void:
	if(isAttached): return # Workaround for stupid signal loop....
	emit_signal("attachEntered", self, other.find_parent("Car*"))
	
	isAttached = true


#func _on_AttachBack_area_entered(other : Area2D) -> void:
#	pass # Replace with function body.
