extends Node2D

export var world_speed : int = 100

export var player_horiz_speed : int = 100


onready var world : Node = $World


var Car : PackedScene = preload("res://Car.tscn")



# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	var world_movement = Vector2()
	
	
	# Player movement ##########
	if(Input.is_action_pressed("ui_left")):
		world_movement.x += player_horiz_speed * delta #NOTE: we're moving the environment right, instead of moving the player left
	
	if(Input.is_action_pressed("ui_right")):
		world_movement.x -= player_horiz_speed * delta
		
	
	# Environment movement ##################
	world_movement.y = world_speed*delta # NOTE: due to int_max, this might put a hard limit on time (but quick calc sets that at several thousand hours at 100px/s)
	
	world.position += world_movement




func _on_Timer_timeout() -> void:
	var inst : Car = Car.instance()
	
	$World/EnemyCars.add_child(inst)
