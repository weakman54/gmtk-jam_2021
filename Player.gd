extends Node2D



func attach(top : Car, bottom : Car) -> void:
#	top.get_node("FrontAttach/Area2D/CollisionShape2D").disabled = true
#	bottom.get_node("BackAttach/Area2D/CollisionShape2D").disabled = true
	
	top.get_node("FrontAttach/Area2D/CollisionShape2D").free()
	bottom.get_node("BackAttach/Area2D/CollisionShape2D").free()
	
#	print(top.get_node("FrontAttach/Area2D/CollisionShape2D"))
#	print(bottom.get_node("BackAttach/Area2D/CollisionShape2D"))
	
	
	
	
	bottom.get_parent().remove_child(bottom)
	top.add_child(bottom)
	
	
	prints(top.position, bottom.position)
	var globalTopAttachFront   = top.position    + top.frontPoint.position
	var globalBottomAttachBack = bottom.position + bottom.backPoint.position
	var pos_diff : Vector2 = globalTopAttachFront - globalBottomAttachBack
	
	prints(globalTopAttachFront, globalBottomAttachBack, pos_diff, bottom.position)
	
	bottom.position += pos_diff



var Car : PackedScene = preload("res://Car.tscn")


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Init #########
	# Spawn initial car
	var inst : Car = Car.instance()
	
	inst.connect("attachEntered", self, "_on_attachEntered")
	
	add_child(inst)


func _on_attachEntered(top : Car, bottom : Car) -> void:
	print("Attach!")
	
	call_deferred("attach", top, bottom)
